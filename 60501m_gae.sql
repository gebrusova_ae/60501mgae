-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Окт 08 2020 г., 14:20
-- Версия сервера: 8.0.21-0ubuntu0.20.04.4
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60501m_gae`
--

-- --------------------------------------------------------

--
-- Структура таблицы `passengers`
--

CREATE TABLE `passengers` (
  `id` int UNSIGNED NOT NULL COMMENT 'ID пассажира',
  `name` varchar(40) NOT NULL COMMENT 'ФИО пассажира '
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Пассажиры';

--
-- Дамп данных таблицы `passengers`
--

INSERT INTO `passengers` (`id`, `name`) VALUES
(1, 'Иванов И.И.'),
(2, 'Петров П.И.'),
(3, 'Сидоров И.С.'),
(4, 'Габбасов Т.С.'),
(5, 'Гареева А.В.'),
(6, 'Гросу А.'),
(7, 'Гебрусова А.Е.'),
(8, 'Лянтин И.С.'),
(9, 'Ниценко М.В.'),
(10, 'Филатов Д.С.');

-- --------------------------------------------------------

--
-- Структура таблицы `point_flights`
--

CREATE TABLE `point_flights` (
  `id` int UNSIGNED NOT NULL,
  `id_route` int UNSIGNED NOT NULL COMMENT 'ID маршрута',
  `id_passenger` int UNSIGNED NOT NULL COMMENT 'ID пассажира',
  `points_spent` int UNSIGNED NOT NULL COMMENT 'Кол-во потраченных баллов',
  `date_time` datetime NOT NULL COMMENT 'Дата и время вылета'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Бонусные полеты';

--
-- Дамп данных таблицы `point_flights`
--

INSERT INTO `point_flights` (`id`, `id_route`, `id_passenger`, `points_spent`, `date_time`) VALUES
(1, 8, 9, 800, '2019-06-19 07:35:00'),
(2, 1, 10, 1000, '2019-06-19 07:35:00'),
(3, 3, 10, 600, '2018-01-19 20:02:48'),
(4, 3, 6, 600, '2018-01-19 20:02:48'),
(5, 8, 2, 800, '2019-06-19 07:35:00'),
(6, 1, 7, 1000, '2019-06-19 07:35:00'),
(7, 3, 4, 600, '2018-01-19 20:02:48'),
(8, 3, 1, 600, '2018-01-19 20:02:48');

-- --------------------------------------------------------

--
-- Структура таблицы `premium_flights`
--

CREATE TABLE `premium_flights` (
  `id` int UNSIGNED NOT NULL,
  `id_route` int UNSIGNED NOT NULL COMMENT 'ID маршрута',
  `id_passenger` int UNSIGNED NOT NULL COMMENT 'ID  пассажира ',
  `points_received` int UNSIGNED NOT NULL COMMENT 'Кол-во полученных баллов',
  `date_time` datetime NOT NULL COMMENT 'Дата и время вылета'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Премиальные полеты';

--
-- Дамп данных таблицы `premium_flights`
--

INSERT INTO `premium_flights` (`id`, `id_route`, `id_passenger`, `points_received`, `date_time`) VALUES
(1, 2, 3, 150, '2019-11-13 20:03:01'),
(2, 3, 8, 100, '2016-03-08 09:29:41'),
(3, 4, 2, 400, '2018-03-01 09:29:43'),
(4, 5, 9, 450, '2017-11-13 20:03:01'),
(5, 6, 5, 220, '2016-11-11 12:10:54'),
(6, 7, 4, 200, '2020-01-11 08:10:21'),
(7, 8, 10, 150, '2017-03-18 05:00:07'),
(8, 9, 6, 500, '2016-12-28 13:27:00'),
(9, 10, 9, 500, '2018-09-19 20:02:53'),
(10, 5, 10, 500, '2012-01-11 08:10:21'),
(11, 9, 7, 500, '2019-09-17 04:32:11'),
(12, 5, 10, 450, '2018-06-12 04:34:08'),
(13, 10, 10, 500, '2018-06-30 04:31:00'),
(14, 9, 10, 500, '2019-10-15 08:00:00'),
(15, 1, 6, 200, '2019-06-19 07:35:00'),
(16, 3, 1, 100, '2015-01-19 20:02:48'),
(17, 1, 1, 200, '2018-01-19 20:02:48'),
(18, 1, 3, 200, '2015-01-19 20:02:48'),
(19, 1, 5, 200, '2019-06-19 07:35:00'),
(20, 1, 9, 200, '2019-01-19 20:02:48'),
(21, 1, 1, 200, '2019-01-19 20:02:48'),
(22, 1, 3, 200, '2018-03-18 05:00:07');

-- --------------------------------------------------------

--
-- Структура таблицы `routes`
--

CREATE TABLE `routes` (
  `id` int UNSIGNED NOT NULL COMMENT 'ID маршрута',
  `rname` varchar(80) NOT NULL,
  `prize_in_points` int UNSIGNED NOT NULL COMMENT 'Премия в баллах за маршрут ',
  `cost_in_points` int UNSIGNED NOT NULL COMMENT 'Стоимость маршрута в баллах'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Маршруты';

--
-- Дамп данных таблицы `routes`
--

INSERT INTO `routes` (`id`, `rname`, `prize_in_points`, `cost_in_points`) VALUES
(1, 'Сургут-Москва', 200, 1000),
(2, 'Сургут-Санкт-Петербург', 150, 850),
(3, 'Сургут-Екатеринбург', 100, 600),
(4, 'Москва-Лондон', 400, 1200),
(5, 'Москва-Париж', 450, 1600),
(6, 'Москва-Сургут', 220, 1200),
(7, 'Санкт-Петербург-Сургут', 200, 1000),
(8, 'Екатеринбург-Сургут', 150, 800),
(9, 'Лондон-Москва', 500, 2200),
(10, 'Париж-Москва', 500, 2000);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `passengers`
--
ALTER TABLE `passengers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `point_flights`
--
ALTER TABLE `point_flights`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_route` (`id_route`),
  ADD KEY `id_passenger` (`id_passenger`);

--
-- Индексы таблицы `premium_flights`
--
ALTER TABLE `premium_flights`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_route` (`id_route`),
  ADD KEY `id_passenger` (`id_passenger`);

--
-- Индексы таблицы `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `passengers`
--
ALTER TABLE `passengers`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID пассажира', AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `point_flights`
--
ALTER TABLE `point_flights`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `premium_flights`
--
ALTER TABLE `premium_flights`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT для таблицы `routes`
--
ALTER TABLE `routes`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID маршрута', AUTO_INCREMENT=11;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `point_flights`
--
ALTER TABLE `point_flights`
  ADD CONSTRAINT `point_flights_ibfk_1` FOREIGN KEY (`id_route`) REFERENCES `routes` (`id`),
  ADD CONSTRAINT `point_flights_ibfk_2` FOREIGN KEY (`id_passenger`) REFERENCES `passengers` (`id`);

--
-- Ограничения внешнего ключа таблицы `premium_flights`
--
ALTER TABLE `premium_flights`
  ADD CONSTRAINT `premium_flights_ibfk_1` FOREIGN KEY (`id_route`) REFERENCES `routes` (`id`),
  ADD CONSTRAINT `premium_flights_ibfk_2` FOREIGN KEY (`id_passenger`) REFERENCES `passengers` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
