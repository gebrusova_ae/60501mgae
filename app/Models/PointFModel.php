<?php namespace App\Models;
use CodeIgniter\Model;
class PointFModel extends Model
{
    protected $table = 'point_flights'; //таблица, связанная с моделью
    protected $allowedFields = ['id_route', 'id_passenger', 'points_spent', 'date_time'];
    public function getPointF($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }

         return $this->select('pf.id, r.rname, p.name, pf.points_spent, pf.date_time')->distinct()->from('point_flights pf')->join('routes r', 'pf.id_route=r.id')->join('passengers p', 'pf.id_passenger=p.id')->where('p.id', $id)->orderBy('pf.date_time')->findAll();
    }

public function getPointFEdit($id)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
         return $this->where(['id' => $id])->first();
    }
}
