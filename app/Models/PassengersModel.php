<?php namespace App\Models;
use CodeIgniter\Model;
class PassengersModel extends Model
{
    protected $table = 'passengers'; //таблица, связанная с моделью
    protected $allowedFields = ['id','name', 'picture_url'];
    public function getPassenger($id = null)
    {
        if (!isset($id)) {
            return $this->orderBy('id')->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

}
