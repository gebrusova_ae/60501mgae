<?php namespace App\Models;

use CodeIgniter\Model;

class RoutesModel extends Model
{
    protected $table = 'routes'; //таблица, связанная с моделью
    protected $allowedFields = ['name', 'prize_in_points', 'cost_in_points'];
    public function returnRoutes()
        {
		  return $this->findAll();
		}
    public function returnCost($id)
        {
            return $this->where('id', $id)->first();
        }
 
}
