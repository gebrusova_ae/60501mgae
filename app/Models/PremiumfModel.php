<?php namespace App\Models;
use CodeIgniter\Model;
class PremiumfModel extends Model
{
    protected $table = 'premium_flights'; //таблица, связанная с моделью
    protected $allowedFields = ['id_route', 'id_passenger', 'points_received', 'date_time'];
    public function getPremiumF($id = null, $search = '')
    {
        $builder=$this->select('pf.id, pf.id_route, r.rname, p.picture_url, p.name, pf.points_received, pf.date_time, pf.id_passenger')->distinct()->from('premium_flights pf')->join('routes r', 'pf.id_route=r.id')->join('passengers p', 'pf.id_passenger=p.id');

        if (!is_null($id)) {
            return $builder->where('pf.id', $id)->first();
        }

         return $builder->like('p.name', $search,'both', null, true)->orlike('r.rname',$search,'both',null,true)->orderBy('pf.id');
    }



}
