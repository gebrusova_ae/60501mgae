<?php namespace App\Controllers;


use App\Models\RoutesModel;



class Routes extends BaseController
{
    public function index() //Обображение всех записей
    {
error_reporting(E_ALL);
   //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new RoutesModel();
        $data ['routes'] = $model->returnRoutes();
        echo view('routes/view', $this->withIon($data));
    }

}    
