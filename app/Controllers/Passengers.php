<?php namespace App\Controllers;


use App\Models\PassengersModel;
use App\Models\PointFModel;
use App\Models\RoutesModel;
use Aws\S3\S3Client;


class Passengers extends BaseController
{
    public function index() //Обображение всех записей
    {
error_reporting(E_ALL);
   //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new PassengersModel();
        $data ['passengers'] = $model->getPassenger();
        echo view('passengers/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
    //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new PointFModel();
        $data ['pointf'] = $model->getPointF($id);
        echo view('passengers/view', $this->withIon($data));
    }
    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
    helper(['form']);
    $data ['validation'] = \Config\Services::validation();
    $model = new PassengersModel();
    $data ['passengers'] = $model->getPassenger();
    $model = new RoutesModel();
    $data ['routes'] = $model->returnRoutes();
    echo view('passengers/create', $this->withIon($data));
    }
    
    public function store()
    {
    helper(['form','url']);

    if ($this->request->getMethod() === 'post' && $this->validate([
            'id_route' => 'required',
            'id_passenger'  => 'required',
            'date_time'  => 'required',
        ]))
    {
        $model = new PointFModel();
        $model1 = new RoutesModel();
        $cost = $model1->returnCost($this->request->getPost('id_route'));
        $model->save([
            'id_route' => $this->request->getPost('id_route'),
            'id_passenger' => $this->request->getPost('id_passenger'),
            'points_spent' => $cost['cost_in_points'],
            'date_time' => $this->request->getPost('date_time'),
        ]);
        session()->setFlashdata('message', lang('Point_flight_create_success'));
        return redirect()->to('/passengers');
    }
    else
    {
        return redirect()->to('/passengers/create')->withInput();
    }
    }

    public function edit($id)
    {
    if (!$this->ionAuth->loggedIn())
    {
        return redirect()->to('/auth/login');
    }
    $model = new PointFModel();

    helper(['form']);
    $model = new PassengersModel();
    $data ['passengers'] = $model->getPassenger();
    $model = new RoutesModel();
    $data ['routes'] = $model->returnRoutes();
    $model = new PointFModel();
    $data ['pointf'] = $model->getPointFEdit($id);
    $data ['validation'] = \Config\Services::validation();
    echo view('passengers/edit', $this->withIon($data));

    }

    public function update()
    {
    helper(['form','url']);
    echo '/passengers/edit/'.$this->request->getPost('id');
    if ($this->request->getMethod() === 'post' && $this->validate([
            'id'  => 'required',
            'id_route' => 'required',
            'id_passenger'  => 'required',
            'date_time'  => 'required',
        ]))
    { 
        $model = new PointFModel();
        $model1 = new RoutesModel();
        $cost = $model1->returnCost($this->request->getPost('id_route'));
        $model->save([
            'id' => $this->request->getPost('id'),
            'id_route' => $this->request->getPost('id_route'),
            'id_passenger' => $this->request->getPost('id_passenger'),
            'points_spent' => $cost['cost_in_points'],
            'date_time' => $this->request->getPost('date_time'),
        ]);
        //session()->setFlashdata('message', lang('Curating.rating_update_success'));

        return redirect()->to('/passengers');
    }
    else
    {
       return redirect()->to('/passengers/edit/'.$this->request->getPost('id'))->withInput();
    }
    }

    public function delete($id)
    {
    if (!$this->ionAuth->loggedIn())
    {
        return redirect()->to('/auth/login');
    }
    $model = new PointFModel();
    $model-> where('id', $id)->delete();
    return redirect()->to('/passengers');
    }

    public function create_passenger()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
    helper(['form']);
    $data ['validation'] = \Config\Services::validation();
    $model = new PassengersModel();
    echo view('passengers/create_passenger', $this->withIon($data));
    }
    
    public function store_pas()
    {
    helper(['form','url']);

    if ($this->request->getMethod() === 'post' && $this->validate([
            'name' => 'required',
            'picture'  => 'is_image[picture]|max_size[picture,1024]',
        ]))
    {
        $insert = null;
        //получение загруженного файла из HTTP-запроса
        $file = $this->request->getFile('picture');
        if ($file->getSize() != 0) {
            //подключение хранилища
            $s3 = new S3Client([
                'version' => 'latest',
                'region' => 'us-east-1',
                'endpoint' => getenv('S3_ENDPOINT'),
                'use_path_style_endpoint' => true,
                'credentials' => [
                    'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                    'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                ],
            ]);
            //получение расширения имени загруженного файла
            $ext = explode('.', $file->getName());
            $ext = $ext[count($ext) - 1];
            //загрузка файла в хранилище
            $insert = $s3->putObject([
                'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                //генерация случайного имени файла
                'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                'Body' => fopen($file->getRealPath(), 'r+')
            ]);

        }
        $model = new PassengersModel();
        $data = ([
            'name' => $this->request->getPost('name'),
        ]);
        if (!is_null($insert))
            $data['picture_url'] = $insert['ObjectURL'];
        $model->save($data);
        session()->setFlashdata('message', lang('Passenger_create_success'));
        return redirect()->to('/passengers');
    }
    else
    {
        return redirect()->to('/passengers/create_passenger')->withInput();
    }
    }

 public function edit_passenger($id)
    {
    if (!$this->ionAuth->loggedIn())
    {
        return redirect()->to('/auth/login');
    }
    $model = new PassengersModel();
    helper(['form']);
    $data ['passenger'] = $model->getPassenger($id);
    $data ['validation'] = \Config\Services::validation();
    echo view('passengers/edit_passenger', $this->withIon($data));

    }

    public function update_pas()
    {
    helper(['form','url']);
    echo '/passengers/edit_passenger/'.$this->request->getPost('id');
    if ($this->request->getMethod() === 'post' && $this->validate([
            'id'  => 'required',
            'name' => 'required',
            'picture'  => 'is_image[picture]|max_size[picture,1024]'
        ]))
    { 
        $insert = null;
        //получение загруженного файла из HTTP-запроса
        $file = $this->request->getFile('picture');
        if ($file->getSize() != 0) {
            //подключение хранилища
            $s3 = new S3Client([
                'version' => 'latest',
                'region' => 'us-east-1',
                'endpoint' => getenv('S3_ENDPOINT'),
                'use_path_style_endpoint' => true,
                'credentials' => [
                    'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                    'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                ],
            ]);
            //получение расширения имени загруженного файла
            $ext = explode('.', $file->getName());
            $ext = $ext[count($ext) - 1];
            //загрузка файла в хранилище
            $insert = $s3->putObject([
                'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                //генерация случайного имени файла
                'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                'Body' => fopen($file->getRealPath(), 'r+')
            ]);
        }
        $model = new PassengersModel();
        $data = ([
            'id' => $this->request->getPost('id'),
            'name' => $this->request->getPost('name'),
        ]);
        if (!is_null($insert))
            $data['picture_url'] = $insert['ObjectURL'];
        $model->save($data);
        //session()->setFlashdata('message', lang('Curating.rating_update_success'));

        return redirect()->to('/passengers');
    }
    else
    {
       return redirect()->to('/passengers/edit_passenger/'.$this->request->getPost('id'))->withInput();
    }
    }
}

