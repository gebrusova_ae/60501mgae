<?php namespace App\Controllers;


use App\Models\PremiumfModel;
use App\Models\PassengersModel;
use App\Models\RoutesModel;



class Premiumf extends BaseController
{
    public function index() //Обображение всех записей
    {
           if ($this->ionAuth->isAdmin())
        {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            }
            else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;
            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search')))
            {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            }
            else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;
            helper(['form','url']);
            $model = new PremiumFModel();
            $data['premiumf'] = $model->getPremiumF(null, $search)->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;
            echo view('premiumf/view', $this->withIon($data));
        }
        else
        {
            session()->setFlashdata('message', lang('Необходимы права администратора'));
            return redirect()->to('/auth/login');
        }
    }

public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
    helper(['form']);
    $data ['validation'] = \Config\Services::validation();
    $model = new PassengersModel();
    $data ['passengers'] = $model->getPassenger();
    $model = new RoutesModel();
    $data ['routes'] = $model->returnRoutes();
    echo view('premiumf/create', $this->withIon($data));
    }
    
    public function store()
    {
    helper(['form','url']);

    if ($this->request->getMethod() === 'post' && $this->validate([
            'id_route' => 'required',
            'id_passenger'  => 'required',
            'date_time'  => 'required',
        ]))
    {
        $model = new PremiumfModel();
        $model1 = new RoutesModel();
        $cost = $model1->returnCost($this->request->getPost('id_route'));
        $model->save([
            'id_route' => $this->request->getPost('id_route'),
            'id_passenger' => $this->request->getPost('id_passenger'),
            'points_received' => $cost['prize_in_points'],
            'date_time' => $this->request->getPost('date_time'),
        ]);
        session()->setFlashdata('message', lang('Premium_flight_create_success'));
        return redirect()->to('/premiumf');
    }
    else
    {
        return redirect()->to('/premiumf/create')->withInput();
    }
    }

    public function edit($id)
    {
    if (!$this->ionAuth->loggedIn())
    {
        return redirect()->to('/auth/login');
    }
    $model = new PremiumfModel();

    helper(['form']);
    $model = new PassengersModel();
    $data ['passengers'] = $model->getPassenger();
    $model = new RoutesModel();
    $data ['routes'] = $model->returnRoutes();
    $model = new PremiumfModel();
    $data ['premiumf'] = $model->getPremiumF($id);
    $data ['validation'] = \Config\Services::validation();
    echo view('premiumf/edit', $this->withIon($data));

    }

    public function update()
    {
    helper(['form','url']);
    echo '/premiumf/edit/'.$this->request->getPost('id');
    if ($this->request->getMethod() === 'post' && $this->validate([
            'id'  => 'required',
            'id_route' => 'required',
            'id_passenger'  => 'required',
            'date_time'  => 'required',
        ]))
    { 
        $model = new PremiumfModel();
        $model1 = new RoutesModel();
        $cost = $model1->returnCost($this->request->getPost('id_route'));
        $model->save([
            'id' => $this->request->getPost('id'),
            'id_route' => $this->request->getPost('id_route'),
            'id_passenger' => $this->request->getPost('id_passenger'),
            'points_received' => $cost['prize_in_points'],
            'date_time' => $this->request->getPost('date_time'),
        ]);
        //session()->setFlashdata('message', lang('Curating.rating_update_success'));

        return redirect()->to('/premiumf');
    }
    else
    {
       return redirect()->to('/premiumf/edit/'.$this->request->getPost('id'))->withInput();
    }
    }

    public function delete($id)
    {
    if (!$this->ionAuth->loggedIn())
    {
        return redirect()->to('/auth/login');
    }
    $model = new PremiumfModel();
    $model-> where('id', $id)->delete();
    return redirect()->to('/premiumf');
    }
}    
