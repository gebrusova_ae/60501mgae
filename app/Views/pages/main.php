<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<main class="text-center">
<div class="jumbotron">
<img class="mb-4" src="https://www.flaticon.com/premium-icon/icons/svg/870/870194.svg" alt="" width="72" height="72"><h1 class="display-4">Бонусная программа</h1>
<p class="lead">
<p>Добро пожаловать на сайт Авиакомпании!
   Данное приложение позволит узнавать информацию о бонусной программе Авиакомпании.</p>
<a class="btn btn-primary btn-lg" href="#" role="button">Войти</a>
</div>
</main>
<?= $this->endSection() ?>
