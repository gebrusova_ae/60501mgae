<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php echo '<main role="main" class="container">'; ?>
	<h2> Маршруты </h2> 	
	<?php if (!empty($routes)) :
	?>	
   <table class="table">
        <thead class="text-white bg-primary">
        <tr>
            <th scope="col">Номер маршрута</th>
			  <th scope="col">Наименование маршрута</th>
            <th class="text-center" scope="col">Бонусов будет начислено</th>
            <th scope="col">Стоимость в бонусных баллах</th>
        </tr>
        </thead>
        <tbody>
		<?php foreach ($routes as $routes_item): ?>
		<tr>
            <th><?php echo $routes_item['id']?></th>
            <td><?php echo $routes_item['rname']?></td>
            <td class="text-center"><?php echo $routes_item['prize_in_points']?></td>
            <td class="text-center"><?php echo $routes_item['cost_in_points']?></td>
			</form>
        </tr>
		<?php endforeach; ?>
        </tbody>
    </table>
 <a href="<?= base_url()?>" class="btn btn-primary">Назад</a>
 <?php else : ?>
        <p> Маршруты не найдены.</p>
    <?php endif ?>
<?= $this->endSection() ?>
		

