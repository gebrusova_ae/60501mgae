<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('passengers/store'); ?>
    <div class="form-group">
       <label for="id_route">Маршрут</label>
        <select  id="id_route" class="form-control <?= ($validation->hasError('id_route')) ? 'is-invalid' : ''; ?>" name="id_route"  value="<?= old('id_route'); ?>">
            <?php
		    foreach ($routes as $route_item):
            {
                echo "<option value=".$route_item['id'].">".$route_item['rname']."</option>";
            }
			endforeach;
			?>
    </select>
        <div class="invalid-feedback">
            <?= $validation->getError('id_route') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="id_passenger">Пассажир</label>
        <select id="id_passenger" class="form-control <?= ($validation->hasError('id_passenger')) ? 'is-invalid' : ''; ?>" name="id_passenger" value="<?= old('id_passenger'); ?>" >  
		<?php
          foreach ($passengers as $passenger_item):
            {
                echo "<option value=".$passenger_item['id'].">".$passenger_item['name']."</option>";
            }
			endforeach;
	    ?>	
        </select>
        <div class="invalid-feedback">
            <?= $validation->getError('id_passenger') ?>
        </div>
    </div>

        <div class="form-group">
            <label for="date_time">Дата и время вылета</label>
            <input type="datetime-local" class="form-control <?= ($validation->hasError('date_time')) ? 'is-invalid' : ''; ?>" name="date_time" value="<?= old('date_time'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('date_time') ?>
            </div>
        </div>

    <div class="form-group">
    <button type="submit" class="btn btn-primary" name="submit">Создать</button>
    </div>
    </form>


    </div>
<?= $this->endSection() ?>
