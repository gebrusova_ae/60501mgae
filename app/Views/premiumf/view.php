<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php echo '<main role="main" class="container">'; ?>
	<h2> Премиальные полеты </h2> 	
	<?php if (!empty($premiumf)) :
	?>	
    <div class="d-flex justify-content-between mb-2">
   <?= $pager->links('group1','my_page') ?>
<?= form_open('premiumf', ['style' => 'display: flex']); ?>
        <select name="per_page" class="ml-3" aria-label="per_page">
            <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
            <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
            <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
            <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
        </select>
        <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
        </form>
        <?= form_open('premiumf',['style' => 'display: flex']); ?>
        <input type="text" class="form-control ml-3" name="search" placeholder="Пассажир или Маршрут" aria-label="Search"
               value="<?= $search; ?>">
        <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
        </form>
    </div>
   <table class="table">
        <thead class="text-white bg-primary">
        <tr>
            <th scope="col">Номер полета</th>
			<th scope="col">Наименование маршрута</th>
            <th scope="col">Аватар</th>
            <th scope="col">ФИО пассажира</th>
            <th class="text-center" scope="col">Бонусов начислено</th>
            <th scope="col">Дата и время полета</th>
            <th scope="col">Действия</th>
        </tr>
        </thead>
        <tbody>
		<?php foreach ($premiumf as $item): ?>
		<tr>
            <th><?php echo $item['id']?></th>
            <td><?php echo $item['rname']?></td>
            <td> <?php if (is_null($item['picture_url'])) : ?>
                          <img height="50" src="https://www.flaticon.com/svg/static/icons/svg/1077/1077012.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                        <?php else:?>
                            <img height="50" src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['name']); ?>">
                        <?php endif ?> </td> 
            <td><?php echo $item['name']?></td>
            <td class="text-center"><?php echo $item['points_received']?></td>
            <td class="text-center"><?php echo $item['date_time']?></td>
            <td>
                <a href="<?= base_url()?>/passengers/view/<?= esc($item['id_passenger']); ?>" class="btn btn-primary btn-sm">Просмотреть бонусный баланс пассажира</a>
                <a href="<?= base_url()?>/premiumf/edit/<?= esc($item['id']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                <a href="<?= base_url()?>/premiumf/delete/<?= esc($item['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
            </td>
			</form>
        </tr>
		<?php endforeach; ?>
        </tbody>
    </table>
 <a href="<?= base_url()?>" class="btn btn-primary">Назад</a>
 <?php else : ?>
        <p> Премиальные полеты не найдены.</p>
    <?php endif ?>
<?= $this->endSection() ?>
		

