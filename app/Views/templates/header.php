<html>
<head>
    <title>Авиакомпания</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/6e9b058a28.js"></script> 
    <meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
        main {
            margin-top: 90px;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary rounded" >
    <a class="navbar-brand" href="<?= base_url()?>">Авиакомпания</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample09" aria-controls="navbarsExample09" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExample09">
      <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown show">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Пассажиры</a>
			<div class="dropdown-menu" aria-labelledby="dropdown01">
            <a class="dropdown-item" href="http://60501mgae.student.surgu.ru/passengers">Просмотр бонусного баланса</a>
            </div>
          </li>
            <li class="nav-item dropdown show">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Маршруты</a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
				<a class="dropdown-item" href="http://60961gae.surgu.site/routes/index">Просмотр маршрутов</a>
                </div>
            </li>
            <li class="nav-item dropdown show">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Полеты</a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">				
                    <a class="dropdown-item" href="http://60961gae.surgu.site/premium_flights/index">Просмотр премиальных полетов</a>
                    <a class="dropdown-item" href="<?= base_url()?>/passengers/store">Добавить пассажира</a>
                </div>
				   </li>
				   </ul>
   <ul class="navbar-nav">
        <?php if (! $ionAuth->loggedIn()): ?>
            <div class="nav-item dropdown">
              <a class="nav-link active" href="<?= base_url()?>/auth/login"><span class="fas fa fa-sign-in-alt" style="color:white"></span>&nbsp;&nbsp;Вход</a>
            </div>
        <?php else: ?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" ><span class="fas fa fa-user-alt" style="color:white"></span>&nbsp;&nbsp;  <?php echo $ionAuth->user()->row()->email; ?></a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
                    <a class="dropdown-item" href="<?= base_url()?>/auth/logout"><span class="fas fa fa-sign-in-alt" style="color:white"></span>&nbsp;&nbsp;Выход</a>
                </div>
            </li>
        <?php endif ?>
        </ul>
    </div>
    </nav>
