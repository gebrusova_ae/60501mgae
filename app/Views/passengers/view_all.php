<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
<h2>Все пассажиры</h2>

<?php if (!empty($passengers) && is_array($passengers)) : ?>

    <?php foreach ($passengers as $item): ?>

        <div class="card mb-3" style="max-width: 540px;">
            <div class="row">
                <div class="col-md-4 d-flex align-items-center">
                <?php if (is_null($item['picture_url'])) : ?>
                          <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/1077/1077012.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                        <?php else:?>
                            <img height="150" src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['name']); ?>">
                        <?php endif ?>                    
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title"><?= esc($item['name']); ?></h5>
                        
                        <a href="<?= base_url()?>/index.php/passengers/view/<?= esc($item['id']); ?>" class="btn btn-primary">Бонусные полеты</a>
                        <a href="<?= base_url()?>/index.php/passengers/edit_passenger/<?php echo $item['id']?>" class="btn btn-primary">Редактировать</a>
                        <p class="card-text"><small class="text-muted">100</small></p>
                    </div>
                </div>
            </div>
        </div>

    <?php endforeach; ?>

<?php else : ?>

    <p>Невозможно найти пассажиров.</p>

<?php endif ?>
</div>
<?= $this->endSection() ?>
