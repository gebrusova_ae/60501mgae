<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php echo '<main role="main" class="container">'; ?>
	<h2> Бонусный баланс: 100 </h2> 	
	<?php if (!empty($pointf)) :
	?>	
   <table class="table">
        <thead class="text-white bg-primary">
        <tr>
            <th scope="col">Наименование маршрута</th>
			  <th scope="col">ФИО пассажира</th>
            <th class="text-center" scope="col">Количество потраченных баллов</th>
            <th scope="col">Дата и время вылета</th>
			<th scope="col">   </th>
        </tr>
        </thead>
        <tbody>
		<?php foreach ($pointf as $point_item): ?>
		<tr>
            <th><?php echo $point_item['rname']?></th>
            <td><?php echo $point_item['name']?></td>
            <td class="text-center"><?php echo $point_item['points_spent']?></td>
            <td><?php echo $point_item['date_time']?></td>
			</form>
        </tr>
<td><a href="<?= base_url()?>/index.php/passengers/edit/<?php echo $point_item['id']?>" class="btn btn-primary">Редактировать</a></td>
            <td><a href="<?= base_url()?>/index.php/passengers/delete/<?php echo $point_item['id']?>" class="btn btn-danger">Удалить</a></td>
		<?php endforeach; ?>
        </tbody>
    </table>
 <a href="<?= base_url()?>/index.php/passengers" class="btn btn-primary">Назад</a>
 <?php else : ?>
        <p> Бонусные полеты не найдены.</p>
    <?php endif ?>
<?= $this->endSection() ?>
		

