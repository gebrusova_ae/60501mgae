<?php

namespace App\Services;

use Google_Client;

class GoogleClient
{
    private $google_client;
    public function __construct()
    {
        $this->google_client = new Google_Client();
        $this->google_client->setClientId('622735893944-g5esrlnd1r29tglrm9h486vftc7fd53t.apps.googleusercontent.com');
        $this->google_client->setClientSecret('s74zjn9DibqQlgf4M4stM4-g');
        $this->google_client->setRedirectUri(base_url().'/auth/google_login');
        $this->google_client->addScope('email');
        $this->google_client->addScope('profile');
    }
    public function getGoogleClient()
    {
        return $this->google_client;
    }

}
