<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class GAE60501m extends Seeder
{
   public function run()
   {
        $data = [
                'id' => '1',
                'name'=>'Иванов И.И.',
        ];
        $this->db->table('passengers')->insert($data);

        $data = [
                'id' => '2',
                'name'=>'Петров П.И.',
        ];
        $this->db->table('passengers')->insert($data);

        $data = [
                'id' => '3',
                'name'=>'Сидоров И.С.',
        ];
        $this->db->table('passengers')->insert($data);

        $data = [
                'id' => '4',
                'name'=>'Габбасов Т.С.',
        ];
        $this->db->table('passengers')->insert($data);

        $data = [
                'id' => '5',
                'name'=>'Гареева А.В.',
        ];
        $this->db->table('passengers')->insert($data);

         $data = [
                'id' => '6',
                'name'=>'Гросу А.',
        ];
        $this->db->table('passengers')->insert($data);

        $data = [
                'id' => '7',
                'name'=>'Гебрусова А.Е.',
        ];
        $this->db->table('passengers')->insert($data);
        
        $data = [
                'id' => '8',
                'name'=>'Лянтин И.С.',
        ];
        $this->db->table('passengers')->insert($data);

        $data = [
                'id' => '9',
                'name'=>'Ниценко М.В.',
        ];
        $this->db->table('passengers')->insert($data);

        $data = [
                'id' => '10',
                'name'=>'Филатов Д.С.',
        ];
        $this->db->table('passengers')->insert($data);

        $data = [
                'id' => '1',
                'rname'=>'Сургут-Москва',
                'prize_in_points' => '200',
                'cost_in_points' => '1000',
        ];
        $this->db->table('routes')->insert($data);

        $data = [
                'id' => '2',
                'rname'=>'Сургут-Санкт-Петербург',
                'prize_in_points' => '150',
                'cost_in_points' => '850',
        ];
        $this->db->table('routes')->insert($data);

        $data = [
                'id' => '3',
                'rname'=>'Сургут-Екатеринбург',
                'prize_in_points' => '100',
                'cost_in_points' => '600',
        ];
        $this->db->table('routes')->insert($data);

        $data = [
                'id' => '4',
                'rname'=>'Москва-Лондон',
                'prize_in_points' => '400',
                'cost_in_points' => '1200',
        ];
        $this->db->table('routes')->insert($data);

        $data = [
                'id' => '5',
                'rname'=>'Москва-Париж',
                'prize_in_points' => '450',
                'cost_in_points' => '1600',
        ];
        $this->db->table('routes')->insert($data);

        $data = [
                'id' => '6',
                'rname'=>'Москва-Сургут',
                'prize_in_points' => '220',
                'cost_in_points' => '1200',
        ];
        $this->db->table('routes')->insert($data);

        $data = [
                'id' => '7',
                'rname'=>'Санкт-Петербург-Сургут',
                'prize_in_points' => '200',
                'cost_in_points' => '1000',
        ];
        $this->db->table('routes')->insert($data);

        $data = [
                'id' => '8',
                'rname'=>'Екатеринбург-Сургут',
                'prize_in_points' => '150',
                'cost_in_points' => '800',
        ];
        $this->db->table('routes')->insert($data);

        $data = [
                'id' => '9',
                'rname'=>'Лондон-Москва',
                'prize_in_points' => '500',
                'cost_in_points' => '2200',
        ];
        $this->db->table('routes')->insert($data);

        $data = [
                'id' => '10',
                'rname'=>'Париж-Москва',
                'prize_in_points' => '500',
                'cost_in_points' => '2000',
        ];
        $this->db->table('routes')->insert($data);

        $data = [
                'id'=> '1',
                'id_route'=>'8',
                'id_passenger'=>'9',
                'points_spent'=>'800',
                'date_time'=>'2019-06-19 07:35:00',
                
        ];
        $this->db->table('point_flights')->insert($data);

        $data = [
                'id'=> '2',
                'id_route'=>'1',
                'id_passenger'=>'10',
                'points_spent'=>'1000',
                'date_time'=>'2019-06-19 07:35:00',
                
        ];
        $this->db->table('point_flights')->insert($data);

        $data = [
                'id'=> '3',
                'id_route'=>'3',
                'id_passenger'=>'10',
                'points_spent'=>'600',
                'date_time'=>'2018-01-19 20:02:48',
                
        ];
        $this->db->table('point_flights')->insert($data);

        $data = [
                'id'=> '4',
                'id_route'=>'3',
                'id_passenger'=>'6',
                'points_spent'=>'600',
                'date_time'=>'2018-01-19 20:02:48',
                
        ];
        $this->db->table('point_flights')->insert($data);

        $data = [
                'id'=> '5',
                'id_route'=>'8',
                'id_passenger'=>'2',
                'points_spent'=>'800',
                'date_time'=>'2019-06-19 07:35:00',
                
        ];
        $this->db->table('point_flights')->insert($data);

        $data = [
                'id'=> '6',
                'id_route'=>'1',
                'id_passenger'=>'7',
                'points_spent'=>'1000',
                'date_time'=>'2019-06-19 07:35:00',
                
        ];
        $this->db->table('point_flights')->insert($data);

        $data = [
                'id'=> '7',
                'id_route'=>'3',
                'id_passenger'=>'4',
                'points_spent'=>'6000',
                'date_time'=>'2018-01-19 20:02:48',
                
        ];
        $this->db->table('point_flights')->insert($data);

        $data = [
                'id'=> '16',
                'id_route'=>'7',
                'id_passenger'=>'1',
                'points_spent'=>'1000',
                'date_time'=>'2019-06-19 07:35:00',
                
        ];
        $this->db->table('point_flights')->insert($data);       

        $data = [
                'id'=> '1',
                'id_route'=>'2',
                'id_passenger'=>'3',
                'points_received'=>'150',
                'date_time'=>'2019-11-13 20:03:01',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '2',
                'id_route'=>'3',
                'id_passenger'=>'8',
                'points_received'=>'100',
                'date_time'=>'2016-03-08 09:29:41',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '3',
                'id_route'=>'4',
                'id_passenger'=>'2',
                'points_received'=>'400',
                'date_time'=>'2018-03-01 09:29:43',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '4',
                'id_route'=>'5',
                'id_passenger'=>'9',
                'points_received'=>'450',
                'date_time'=>'2017-11-13 20:03:01',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '5',
                 'id_route'=>'6',
                'id_passenger'=>'5',
                'points_received'=>'220',
                'date_time'=>'2016-11-11 12:10:54',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '6',
                'id_route'=>'7',
                'id_passenger'=>'4',
                'points_received'=>'200',
                'date_time'=>'2020-01-11 08:10:21',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '7',
                'id_route'=>'8',
                'id_passenger'=>'10',
                'points_received'=>'150',
                'date_time'=>'2017-03-18 05:00:07',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '8',
                'id_route'=>'9',
                'id_passenger'=>'6',
                'points_received'=>'500',
                'date_time'=>'2016-12-28 13:27:00',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '9',
                'id_route'=>'10',
                'id_passenger'=>'9',
                'points_received'=>'500',
                'date_time'=>'2018-09-19 20:02:53',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '10',
                'id_route'=>'5',
                'id_passenger'=>'10',
                'points_received'=>'500',
                'date_time'=>'2012-01-11 08:10:21',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '11',
                'id_route'=>'9',
                'id_passenger'=>'7',
                'points_received'=>'500',
                'date_time'=>'2019-09-17 04:32:11',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '12',
                'id_route'=>'5',
                'id_passenger'=>'10',
                'points_received'=>'450',
                'date_time'=>'2018-06-12 04:34:08',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '13',
                'id_route'=>'10',
                'id_passenger'=>'10',
                'points_received'=>'500',
                'date_time'=>'2018-06-30 04:31:00',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '14',
                'id_route'=>'9',
                'id_passenger'=>'10',
                'points_received'=>'500',
                'date_time'=>'2019-10-15 08:00:00',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '15',
                 'id_route'=>'1',
                'id_passenger'=>'6',
                'points_received'=>'200',
                'date_time'=>'2019-06-19 07:35:00',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '16',
                'id_route'=>'3',
                'id_passenger'=>'1',
                'points_received'=>'100',
                'date_time'=>'2015-01-19 20:02:48',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '17',
                'id_route'=>'1',
                'id_passenger'=>'1',
                'points_received'=>'200',
                'date_time'=>'2018-01-19 20:02:48',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '18',
                'id_route'=>'1',
                'id_passenger'=>'3',
                'points_received'=>'200',
                'date_time'=>'2015-01-19 20:02:48',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '19',
                'id_route'=>'1',
                'id_passenger'=>'5',
                'points_received'=>'200',
                'date_time'=>'2019-06-19 07:35:00',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '20',
                'id_route'=>'1',
                'id_passenger'=>'9',
                'points_received'=>'200',
                'date_time'=>'2019-01-19 20:02:48',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '21',
                'id_route'=>'1',
                'id_passenger'=>'1',
                'points_received'=>'200',
                'date_time'=>'2019-01-19 20:02:48',
                
        ];
        $this->db->table('premium_flights')->insert($data); 

$data = [
                'id'=> '22',
                'id_route'=>'1',
                'id_passenger'=>'3',
                'points_received'=>'200',
                'date_time'=>'2018-03-18 05:00:07',
                
        ];
        $this->db->table('premium_flights')->insert($data); 







   }
}
