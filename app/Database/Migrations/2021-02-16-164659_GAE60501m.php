<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class GAE60501m extends Migration
{
	public function up()
	{
		 // passengers
        if (!$this->db->tableexists('passengers'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '40', 'null' => FALSE),
            ));
            // create table
            $this->forge->createtable('passengers', TRUE);
        }

         // routes
        if (!$this->db->tableexists('routes'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'rname' => array('type' => 'VARCHAR', 'constraint' => '80', 'null' => FALSE),
                'prize_in_points' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'cost_in_points' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
            ));
            // create table
            $this->forge->createtable('routes', TRUE);
        }

         // point_flights
        if (!$this->db->tableexists('point_flights'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_route' => array('type' => 'int', 'unsigned' => TRUE, 'null' => TRUE),
                'id_passenger' => array('type' => 'int', 'unsigned' => TRUE, 'null' => TRUE),
                'points_spent' => array('type' => 'int', 'unsigned' => TRUE, 'null' => TRUE),
                'date_time' => array('type' => 'DATETIME', 'null' => TRUE),
            ));
            $this->forge->addForeignKey('id_route','routes','id','RESTRICT','RESRICT');
            $this->forge->addForeignKey('id_passenger','passengers','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('point_flights', TRUE);
        }

        // premium_flights
        if (!$this->db->tableexists('premium_flights'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_route' => array('type' => 'int', 'unsigned' => TRUE, 'null' => TRUE),
                'id_passenger' => array('type' => 'int', 'unsigned' => TRUE, 'null' => TRUE),
                'points_received' => array('type' => 'int', 'unsigned' => TRUE, 'null' => TRUE),
                'date_time' => array('type' => 'DATETIME', 'null' => TRUE),
            ));
            $this->forge->addForeignKey('id_route','routes','id','RESTRICT','RESRICT');
            $this->forge->addForeignKey('id_passenger','passengers','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('premium_flights', TRUE);
        } 
	}

	//--------------------------------------------------------------------

	public function down()
	{
        $this->forge->droptable('passengers');
        $this->forge->droptable('routes');
        $this->forge->droptable('point_flights');
        $this->forge->droptable('premium_flights');
	}
}
