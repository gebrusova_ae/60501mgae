<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Addpictureurlfield extends Migration
{
	public function up()
	{
		 if ($this->db->tableexists('passengers'))
        {
            $this->forge->addColumn('passengers',array(
                'picture_url' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE)
            ));
        }
	}

	public function down()
	{
		$this->forge->dropColumn('passengers', 'picture_url');
	}
}
